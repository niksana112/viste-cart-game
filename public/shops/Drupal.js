module.exports = class Drupal {
    constructor(top, left) {
        this.top = -17;
        this.left = left;
        this.tile = 'drupal-tile';
        this.points = 35;
    }
};