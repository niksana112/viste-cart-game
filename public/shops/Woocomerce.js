module.exports = class Woocomerce {
    constructor(top, left) {
        this.top = -10;
        this.left = left;
        this.tile = 'woocomerce-tile';
        this.points = 35;
    }
};
