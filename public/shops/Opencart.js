module.exports = class Opencart {
    constructor(top, left) {
        this.top = 1;
        this.left = left;
        this.tile = 'opencart-tile';
        this.points = 35;
    }
};