module.exports = class FreezePlayer {
    constructor(top, left) {
        this.top = -5;
        this.left = left;
        this.tile = 'freeze-tile';
        this.description = 'Freeze';
    }
    behavior(Event){
        Event.fire('freezePlayer', {});
    }
};