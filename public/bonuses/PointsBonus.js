module.exports = class PointsBonus {
    constructor(top, left) {
        this.top = -22;
        this.left = left;
        this.tile = 'pointsBonus-tile';
    }
    behavior (Event){
        Event.fire('doublePoints', {});
    }
};