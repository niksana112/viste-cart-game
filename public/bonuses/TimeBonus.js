module.exports = class TimeBonus {
    constructor(top, left) {
        this.top = -22;
        this.left = left;
        this.tile = 'timeBonus-tile';
        this.description = "Time Bonus"
    }
    behavior(Event){
        Event.fire('slowDownSpeed', {});
    }
};