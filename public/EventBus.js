// 'playerMoved'
//    - cuk
//    - blink
// let eventList = {
//     'playerMoved': [cyk]
// };

// subscribe('playerMoved', function(positions) { obadi se na yavo})
// subscribe('playerMoved', function(positions) { obadi se na vili })
// subscribe('playerMoved', function(positions) {obadi se na niki})
// subscribe('playerMoved', function(positions) {obadi se na mitko })
// subscribe('playerMoved', function(positions) {})
// subscribe('playerMoved', function(positions) {})
// subscribe('playerMoved', function(positions) {})
//
// fire('playerMoved', { oldtop: 5, oldleft: 7, newtop: 6, newleft: 7});
module.exports = class EventBus {
    constructor() {
        this.eventList = {};
    }

    fire(eventName, payload) {
        if (!this.areThereSubscribersForEvent(eventName)) {
            return;
        }

        let functions = this.eventList[eventName];
        for (let i = 0; i < functions.length; i++) {
            let functionFromEventList = functions[i];
            functionFromEventList(payload);
        }
    }
//  subscribe('playerMoved', function() {});
    subscribe(eventName, func) {
        if (!this.areThereSubscribersForEvent(eventName)) {
            this.eventList[eventName] = [];
        }
        this.eventList[eventName].push(func);
    }

    areThereSubscribersForEvent(eventName) {
        if (!this.eventList[eventName] || !this.eventList[eventName] instanceof Array) {
            return false;
        }
        return true;
    }

    unsubscribe(eventName, func){
        if (!this.areThereSubscribersForEvent(eventName)){
            return;
        }
//      eventList['playerMoved'].indexOf(func); 0/1/2/3, -1
        let functionIndex = this.eventList[eventName].indexOf(func);
        if(functionIndex > -1){
            this.eventList[eventName].splice( functionIndex, 1)
        }
    }
};