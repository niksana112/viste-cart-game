const $ = require('jquery');
module.exports = {
    flyToPlayer(points, player) {
        let divider = 3;
        let pointsClone = $(points).clone();
        $(pointsClone).css({
            position: 'absolute',
            top: $(points).offset().top + "px",
            left: $(points).offset().left + "px",
            opacity: 1,
            'z-index': 2000
        });
        $('body').append($(pointsClone));
        let gotoX = $(player).offset().left + ($(player).width() / 2) - ($(points).width() / divider) / 2;
        let gotoY = $(player).offset().top + ($(player).height() / 2) - ($(points).height() / divider) / 2;

        $(pointsClone).animate({
                opacity: 0.3,
                left: gotoX,
                top: gotoY,
                width: $(points).width() / divider,
                height: $(points).height() / divider
            }, 1200,
            function () {
                $(pointsClone).remove();
            });
    }

};
