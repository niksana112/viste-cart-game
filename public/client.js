const $ = require('jquery');
const Wordpress = require('./shops/Wordpress');
const Opencart = require('./shops/Opencart');
const Drupal = require('./shops/Drupal');
const Joomla = require('./shops/Joomla');
const Woocomerce = require('./shops/Woocomerce');
const Presta = require('./shops/Presta');
const TimeBonus = require('./bonuses/TimeBonus');
const PointsBonus = require('./bonuses/PointsBonus');
const FreezePlayer = require('./antibonuses/FreezePlayer');
const canvas = require('./canvas.json');
const Engine = require('./Engine')(canvas);
const keyDown = require('./keyDown');
const helpers = require('./helpers');
const EventBus = require('./EventBus');
const Event = new EventBus();
const Animations = require('./animations');
let objectClasses = [Opencart, Wordpress, Joomla, Presta, Woocomerce, TimeBonus, PointsBonus, FreezePlayer];
const playgroundHeight = 22;
let divClearBoth = '<div style="clear:both"></div>';

Event.subscribe('pointsChanged', pointsAnimation);
Event.subscribe('slowDownSpeed', slowDownStepDelay);
Event.subscribe('doublePoints', multiplyBonus);
Event.subscribe('freezePlayer', playerFreeze);

function pointsAnimation(payload) {
    let newPointsAdded = document.getElementById('points-added');
    newPointsAdded.innerHTML = payload.points;
    //select point and pass it to the function
    let currentPoints = newPointsAdded;
    Animations.flyToPlayer($(currentPoints), $('#player'));
}

function slowDownStepDelay(payload) {
    stepDelay = 400;
    setTimeout(function () {
        stepDelay = 200;
    }, 10000);
    Event.fire('pointsChanged', {points: "Slow Down"});
}

function multiplyBonus(payload) {
    player.multiplyPoints = true;
    setTimeout(function () {
        player.multiplyPoints = false;
    }, 10000);
    Event.fire('pointsChanged', {points: "x2"});
}

function playerFreeze() {
    player.freeze = true;
    setTimeout(function () {
        player.freeze = false;
    }, 10000);
    Event.fire('pointsChanged', {points: "frost nova"});
}

function handlePlayerCollision(payload) { //{ player, tile: obj}
    if (typeof payload.tile['behavior'] === 'function') {
        payload.tile.behavior(Event);
    }

    if (payload.tile.hasOwnProperty('points')) {
        payload.player.getPoints(payload.tile, updatePoints);
        Event.fire('pointsChanged', {points: payload.tile.points});
    }
}
Event.subscribe('playerCollision', handlePlayerCollision);

let player = require('./Player')(Event);


let shops = [];
let newPoints = '';
function keepObjectsOn(maxObjects) {
    if (shops.length < maxObjects) {
        let difference = maxObjects - shops.length;
        for (let i = 1; i <= difference; i++) {
            shops.push(new objectClasses[helpers.getRandomInteger(0, objectClasses.length - 1)](0, Engine.getRandomWidth()));
        }
    }

    // is player on tile
    for (let k = 0; k < shops.length; k++) {
        let obj = shops[k];
        if (player.position.top === obj.top && player.position.left === obj.left) {
            Event.fire('playerCollision', {player, tile: obj});

        }
    }
}


function destroyObjectsOutOfCanvas() {
    for (let i = 0; i < shops.length; i++) {
        let object = shops[i];
        if (object.top == playgroundHeight) {
            let rewindObject = shops[i];
            rewindObject.top = "-" + Engine.getRandomWidth();
            rewindObject.left = Engine.getRandomWidth();
        }
        redrawCanvas(canvas);
    }
}


let canvasWidth = '';
let canvasHeigth = '';
function redrawCanvas(canvas) {
    let canvasString = '';
    for (let i = 0; i < canvas.length; i++) {
        canvasHeigth = i;
        let line = canvas[i];
        let lineString = '';
        for (let j = 0; j < line.length; j++) {
            let symbol = line[j];
            let tileHtml = Engine.getTileBySymbol(symbol);
            canvasWidth = j;
            if (player.position.top === i && player.position.left === j) {
                tileHtml = player.html;
            }
            let objectsOnTile = Engine.findObjectsOnTile(shops, i, j);
            if (objectsOnTile.length > 0) {
                let classes = objectsOnTile[0].tile;// "opencart-tile takes-points"
                if (objectsOnTile[0].points < 0) {
                    classes = classes + " takes-points";
                }
                tileHtml = '<div class="' + classes + '"></div>';
            }

            lineString += tileHtml;
        }
        canvasString += lineString + divClearBoth;
    }

    $("#myDiv").html(canvasString);
    $("#player")
        .find('.player-position')
        .html('Left: ' + player.position.left + '; Top: ' + player.position.top + ';');
}

let points = '';
let playerPoints = document.getElementById('player-poitns');
function updatePoints() {
    setTimeout(function () {
        points = player.points;
        playerPoints.innerHTML = points;
    }, 1000);

}


redrawCanvas(canvas);


document.onkeydown = function (e) {
    keyDown(e, player, redrawCanvas, canvas);
};

function nextStep() {
    Engine.moveObjectsDown(shops);
    keepObjectsOn(6);
    destroyObjectsOutOfCanvas();
    redrawCanvas(canvas);
}

let stepDelay = 200;
function repeatNextStep() {
    setTimeout(function () {
        nextStep();
        repeatNextStep();
    }, stepDelay);
}

repeatNextStep();

//bonus deto da pada i da namalq skorostta za 10 sec -  done
//bonus deto udwoqwa to4kite za 10 sec - done
//anti-bonus koito te freeze-wa na mqsto za 10 sec..
// anti bonus koito ti obrushta posokite na kop4etata
// antibonus deto da ti dawa 2 puti po-malko to4ki ot polojitelnite
// update the animations
//


//move shops (1) >> player/shop collision >> run behaviors / add points >> (1)