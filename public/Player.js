module.exports = function (Event) {
    return {
        bonuses:{
            multiplyPoints:false,
        },
        antibonuses:{
            freeze:false,
        },
        points: 0,
        position: {
            top: 21,
            left: 9,
        },
        html: '<div class="player-tile"></div>',
        moveRight: function (canvas) {
            let canvasWidth = canvas[0].length;
            if (this.position.left < canvasWidth - 2) {
                if(this.freeze){
                    this.position.left += 0;
                }
                else{
                    this.position.left += 1;
                    Event.fire('playerMoved', {top: 0, left: this.position.left});
                }
            }
        },
        moveLeft: function (canvas) {
            if (this.position.left > 1) {
                if(this.freeze){
                    this.position.left -= 0;
                }
                else{
                    this.position.left -= 1;
                    Event.fire('playerMoved', {top: 0, left: this.position.left});
                }

            }
        },
        getPoints(shop, callback){
            if(this.multiplyPoints){
                this.points += shop.points * 2;
            }
            else{
                this.points += shop.points;
            }
            callback();


        }


    }
};