module.exports = function (canvas) {
    return {
        getRandomWidth() {
            return (Math.random() * this.getCanvasWidth() - 1 | 0) + 1;
        },
        getCanvasWidth(){
            return canvas[0].length - 1;
        },
        getTileBySymbol(symbol){
            let tiles = {
                'Âº': '<div class="ground-tile"></div>',
                'X': '<div class="x-tile"></div>',
            };
            return tiles[symbol];
        },
        findObjectsOnTile(objects, top, left) {
        let list = [];
        for (let i = 0; i < objects.length; i++) {
            let object = objects[i];
            if (object.top === top && object.left === left) {
                list.push(object);
            }
        }
        return list;
    },
        moveObjectsDown(shops) {
        for (let i = 0; i < shops.length; i++) {
            let object = shops[i];
            object.top++;
        }
    }

}
};